    // Preloader

    $(window).load(function() {
      $("#loader .icon").fadeOut();
      $("#loader").fadeOut("slow");
    }); 

jQuery(function($) {
  "use strict";

//jQuery to collapse the navbar on scroll
$(window).on(function() {
    if ($(".navbar").offset().top > 50) {
        $(".navbar-fixed-top").addClass("top-nav-collapse");
    } else {
        $(".navbar-fixed-top").removeClass("top-nav-collapse");
    }
});

//jQuery for page scrolling feature - requires jQuery Easing plugin
  jQuery('a.page-scroll').on('click', function(event) {
        var $anchor = $(this);
          $('html, body').stop().animate({
              scrollTop: $($anchor.attr('href')).offset().top
            }, 1500, 'easeInOutExpo');
        event.preventDefault();
    }); 


    //---------------------------------------------------------------- 

    // Highlight top nav as scrolling occurs

        jQuery('body').scrollspy({
           target: '.navbar-fixed-top'
    })

    // Closes the Responsive Menu Menu Item Click
    $('.navbar-collapse ul li a').on('click', function() {
        $('.navbar-toggle:visible').click();
    });
	

 
// =========== wow aniamation========================

 new WOW().init();
   

  });



